USE [Administrar]
GO
/****** Object:  StoredProcedure [dbo].[ActualizaAlumno]    Script Date: 04/11/2019 16:57:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActualizaAlumno]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ActualizaAlumno]
GO
/****** Object:  StoredProcedure [dbo].[ActualizaProfesor]    Script Date: 04/11/2019 16:57:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActualizaProfesor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ActualizaProfesor]
GO
/****** Object:  StoredProcedure [dbo].[EliminarAlumno]    Script Date: 04/11/2019 16:57:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EliminarAlumno]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EliminarAlumno]
GO
/****** Object:  Table [dbo].[Materia]    Script Date: 04/11/2019 16:57:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Materia]') AND type in (N'U'))
DROP TABLE [dbo].[Materia]
GO
/****** Object:  Table [dbo].[Profesor]    Script Date: 04/11/2019 16:57:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Profesor]') AND type in (N'U'))
DROP TABLE [dbo].[Profesor]
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 04/11/2019 16:57:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Usuario]') AND type in (N'U'))
DROP TABLE [dbo].[Usuario]
GO
/****** Object:  Table [dbo].[Alumno]    Script Date: 04/11/2019 16:57:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Alumno]') AND type in (N'U'))
DROP TABLE [dbo].[Alumno]
GO
/****** Object:  Table [dbo].[Alumno]    Script Date: 04/11/2019 16:57:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Alumno]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Alumno](
	[Id_Alumno] [int] NULL,
	[Nom_Alum] [varchar](50) NULL,
	[Apellidos_Alum] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Alumno] ([Id_Alumno], [Nom_Alum], [Apellidos_Alum]) VALUES (123, N'Alan Eduardo', N'Aguilar Cano')
INSERT [dbo].[Alumno] ([Id_Alumno], [Nom_Alum], [Apellidos_Alum]) VALUES (1234, N'Juan Daniel', N'Aguilar Cano')
INSERT [dbo].[Alumno] ([Id_Alumno], [Nom_Alum], [Apellidos_Alum]) VALUES (12, N'Jose Carmen', N'Moya tovar')
INSERT [dbo].[Alumno] ([Id_Alumno], [Nom_Alum], [Apellidos_Alum]) VALUES (1, N'juna', N'miguel')
/****** Object:  Table [dbo].[Usuario]    Script Date: 04/11/2019 16:57:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Usuario]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Usuario](
	[Id_Usuario] [varchar](50) NULL,
	[Nom_Usuario] [varchar](50) NULL,
	[Account] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[Status_admin] [bit] NULL,
	[Foto] [varchar](500) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Usuario] ([Id_Usuario], [Nom_Usuario], [Account], [Password], [Status_admin], [Foto]) VALUES (N'1', N'Juana Martha Hernadez Sandoval', N'JuanaMart', N'2018', 1, N'C:\Users\Juan Miguel\Desktop\Programacion final\Aplicacion_Final\Aplicacion_Final\Resources\Logo progra.jpg')
INSERT [dbo].[Usuario] ([Id_Usuario], [Nom_Usuario], [Account], [Password], [Status_admin], [Foto]) VALUES (N'2', N'Susana Karina Contreras Jimenez', N'Susanita', N'2017', 1, N'C:\Users\Juan Miguel\Desktop\Programacion final\Aplicacion_Final\Aplicacion_Final\Resources\English logo.jpg')
INSERT [dbo].[Usuario] ([Id_Usuario], [Nom_Usuario], [Account], [Password], [Status_admin], [Foto]) VALUES (N'3', N'Refugio Rubio Hernandez', N'ElRubio', N'2015', 1, N'C:\Users\Juan Miguel\Desktop\Programacion final\Aplicacion_Final\Aplicacion_Final\Resources\ElRubio.png')
INSERT [dbo].[Usuario] ([Id_Usuario], [Nom_Usuario], [Account], [Password], [Status_admin], [Foto]) VALUES (N'4', N'Juan Miguel Gomez Olvera', N'Mike', N'2000', 0, N'C:\Users\Juan Miguel\Desktop\Programacion final\Aplicacion_Final\Aplicacion_Final\Resources\Juan.jpg')
/****** Object:  Table [dbo].[Profesor]    Script Date: 04/11/2019 16:57:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Profesor]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Profesor](
	[Id_Profesor] [int] NULL,
	[Nom_profesor] [varchar](50) NULL,
	[Apellidos_Profesor] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Profesor] ([Id_Profesor], [Nom_profesor], [Apellidos_Profesor]) VALUES (1, N'juangomez', N'')
/****** Object:  Table [dbo].[Materia]    Script Date: 04/11/2019 16:57:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Materia]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Materia](
	[Id_Materia] [int] NULL,
	[Nom_Materia] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Materia] ([Id_Materia], [Nom_Materia]) VALUES (1, N'mate')
INSERT [dbo].[Materia] ([Id_Materia], [Nom_Materia]) VALUES (12, N'Progra')
INSERT [dbo].[Materia] ([Id_Materia], [Nom_Materia]) VALUES (12, N'mate')
/****** Object:  StoredProcedure [dbo].[EliminarAlumno]    Script Date: 04/11/2019 16:57:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EliminarAlumno]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create procedure [dbo].[EliminarAlumno]

@id_alumno int
as

delete from Alumno  where Id_Alumno=@id_alumno

' 
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizaProfesor]    Script Date: 04/11/2019 16:57:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActualizaProfesor]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create procedure [dbo].[ActualizaProfesor]

@id_Pro int , @Nom_Pro varchar(50), @Apellidos_Pro varchar(50)

as
--Actualiza Alumno
  (SELECT  Id_Profesor FROM Profesor where Id_Profesor = @id_Pro)

' 
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizaAlumno]    Script Date: 04/11/2019 16:57:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActualizaAlumno]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE procedure [dbo].[ActualizaAlumno]

@id_Alumno int , @Nom_Alumno varchar(50), @Apellidos_alumno varchar(50)

as
--Actualiza Alumno
if not EXISTS (SELECT id_Alumno FROM Alumno where Id_Alumno=@id_Alumno)
insert into Alumno(Id_Alumno,Nom_Alum,Apellidos_Alum) values(@id_Alumno,@Nom_Alumno,@Apellidos_alumno)
else
update Alumno set Id_Alumno=@id_Alumno,Nom_Alum =@Nom_Alumno,Apellidos_Alum=@Apellidos_alumno WHERE Id_Alumno=@id_Alumno' 
END
GO
