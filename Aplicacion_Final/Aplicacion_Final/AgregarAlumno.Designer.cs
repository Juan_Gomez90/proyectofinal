﻿namespace Aplicacion_Final
{
    partial class AgregarAlumno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblId = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtApelliAlum = new MiLibreria.ErrorTxtBox();
            this.txtNomAlum = new MiLibreria.ErrorTxtBox();
            this.txtIdAlum = new MiLibreria.ErrorTxtBox();
            this.SuspendLayout();
            // 
            // btnNuevo
            // 
            this.btnNuevo.Location = new System.Drawing.Point(442, 213);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(442, 150);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(442, 89);
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(442, 33);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(442, 275);
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblId.Location = new System.Drawing.Point(59, 60);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(123, 24);
            this.lblId.TabIndex = 8;
            this.lblId.Text = "ID Alumno:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 24);
            this.label1.TabIndex = 9;
            this.label1.Text = "Nombre alumno:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(-3, 186);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(185, 24);
            this.label2.TabIndex = 10;
            this.label2.Text = "Apellidos Alumno:";
            // 
            // txtApelliAlum
            // 
            this.txtApelliAlum.Location = new System.Drawing.Point(188, 186);
            this.txtApelliAlum.Name = "txtApelliAlum";
            this.txtApelliAlum.Size = new System.Drawing.Size(213, 20);
            this.txtApelliAlum.TabIndex = 13;
            this.txtApelliAlum.Validar = true;
            // 
            // txtNomAlum
            // 
            this.txtNomAlum.Location = new System.Drawing.Point(188, 133);
            this.txtNomAlum.Name = "txtNomAlum";
            this.txtNomAlum.Size = new System.Drawing.Size(213, 20);
            this.txtNomAlum.TabIndex = 12;
            this.txtNomAlum.Validar = true;
            // 
            // txtIdAlum
            // 
            this.txtIdAlum.Location = new System.Drawing.Point(188, 65);
            this.txtIdAlum.Name = "txtIdAlum";
            this.txtIdAlum.Size = new System.Drawing.Size(213, 20);
            this.txtIdAlum.TabIndex = 11;
            this.txtIdAlum.Validar = true;
            this.txtIdAlum.TextChanged += new System.EventHandler(this.txtIdAlum_TextChanged);
            // 
            // AgregarAlumno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(576, 336);
            this.Controls.Add(this.txtApelliAlum);
            this.Controls.Add(this.txtNomAlum);
            this.Controls.Add(this.txtIdAlum);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblId);
            this.Name = "AgregarAlumno";
            this.Text = "AgregarAlumno";
            this.Controls.SetChildIndex(this.lblId, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.txtIdAlum, 0);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.btnNuevo, 0);
            this.Controls.SetChildIndex(this.btnEliminar, 0);
            this.Controls.SetChildIndex(this.btnGuardar, 0);
            this.Controls.SetChildIndex(this.btnConsultar, 0);
            this.Controls.SetChildIndex(this.txtNomAlum, 0);
            this.Controls.SetChildIndex(this.txtApelliAlum, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private MiLibreria.ErrorTxtBox txtIdAlum;
        private MiLibreria.ErrorTxtBox txtNomAlum;
        private MiLibreria.ErrorTxtBox txtApelliAlum;
    }
}