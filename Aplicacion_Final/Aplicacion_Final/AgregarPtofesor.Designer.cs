﻿namespace Aplicacion_Final
{
    partial class AgregarPtofesor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtApelliProfesor = new MiLibreria.ErrorTxtBox();
            this.txtNomProfesor = new MiLibreria.ErrorTxtBox();
            this.txtIdProfesor = new MiLibreria.ErrorTxtBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblId = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(448, 142);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(448, 97);
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(448, 52);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Location = new System.Drawing.Point(448, 192);
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(448, 243);
            // 
            // txtApelliProfesor
            // 
            this.txtApelliProfesor.Location = new System.Drawing.Point(196, 193);
            this.txtApelliProfesor.Name = "txtApelliProfesor";
            this.txtApelliProfesor.Size = new System.Drawing.Size(213, 20);
            this.txtApelliProfesor.TabIndex = 19;
            this.txtApelliProfesor.Validar = true;
            // 
            // txtNomProfesor
            // 
            this.txtNomProfesor.Location = new System.Drawing.Point(196, 140);
            this.txtNomProfesor.Name = "txtNomProfesor";
            this.txtNomProfesor.Size = new System.Drawing.Size(213, 20);
            this.txtNomProfesor.TabIndex = 18;
            this.txtNomProfesor.Validar = true;
            // 
            // txtIdProfesor
            // 
            this.txtIdProfesor.Location = new System.Drawing.Point(196, 72);
            this.txtIdProfesor.Name = "txtIdProfesor";
            this.txtIdProfesor.Size = new System.Drawing.Size(213, 20);
            this.txtIdProfesor.TabIndex = 17;
            this.txtIdProfesor.Validar = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 193);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(189, 24);
            this.label2.TabIndex = 16;
            this.label2.Text = "Apellidos Profesor:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(25, 135);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 24);
            this.label1.TabIndex = 15;
            this.label1.Text = "Nombre Profesor:";
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblId.Location = new System.Drawing.Point(67, 67);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(127, 24);
            this.lblId.TabIndex = 14;
            this.lblId.Text = "ID Profesor:";
            // 
            // AgregarPtofesor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 301);
            this.Controls.Add(this.txtApelliProfesor);
            this.Controls.Add(this.txtNomProfesor);
            this.Controls.Add(this.txtIdProfesor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblId);
            this.Name = "AgregarPtofesor";
            this.Text = "AgregarPtofesor";
            this.Controls.SetChildIndex(this.btnNuevo, 0);
            this.Controls.SetChildIndex(this.btnEliminar, 0);
            this.Controls.SetChildIndex(this.btnGuardar, 0);
            this.Controls.SetChildIndex(this.btnConsultar, 0);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.lblId, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.txtIdProfesor, 0);
            this.Controls.SetChildIndex(this.txtNomProfesor, 0);
            this.Controls.SetChildIndex(this.txtApelliProfesor, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MiLibreria.ErrorTxtBox txtApelliProfesor;
        private MiLibreria.ErrorTxtBox txtNomProfesor;
        private MiLibreria.ErrorTxtBox txtIdProfesor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblId;
    }
}