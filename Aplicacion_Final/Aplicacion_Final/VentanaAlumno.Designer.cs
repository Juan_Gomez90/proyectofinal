﻿namespace Aplicacion_Final
{
    partial class VentanaAlumno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblUsAlum = new System.Windows.Forms.Label();
            this.lblCuenta = new System.Windows.Forms.Label();
            this.lblcod = new System.Windows.Forms.Label();
            this.lblCodAlum = new System.Windows.Forms.Label();
            this.lblNomAlum = new System.Windows.Forms.Label();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.btnContenedor = new System.Windows.Forms.Button();
            this.picImagen = new System.Windows.Forms.PictureBox();
            this.lblAlumno = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picImagen)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(351, 267);
            this.btnSalir.Size = new System.Drawing.Size(121, 23);
            // 
            // lblUsAlum
            // 
            this.lblUsAlum.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblUsAlum.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUsAlum.Location = new System.Drawing.Point(296, 98);
            this.lblUsAlum.Name = "lblUsAlum";
            this.lblUsAlum.Size = new System.Drawing.Size(153, 28);
            this.lblUsAlum.TabIndex = 24;
            // 
            // lblCuenta
            // 
            this.lblCuenta.AutoSize = true;
            this.lblCuenta.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCuenta.Location = new System.Drawing.Point(200, 98);
            this.lblCuenta.Name = "lblCuenta";
            this.lblCuenta.Size = new System.Drawing.Size(90, 24);
            this.lblCuenta.TabIndex = 23;
            this.lblCuenta.Text = "Usuario:";
            // 
            // lblcod
            // 
            this.lblcod.AutoSize = true;
            this.lblcod.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcod.Location = new System.Drawing.Point(213, 153);
            this.lblcod.Name = "lblcod";
            this.lblcod.Size = new System.Drawing.Size(77, 24);
            this.lblcod.TabIndex = 22;
            this.lblcod.Text = "Codigo:";
            // 
            // lblCodAlum
            // 
            this.lblCodAlum.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblCodAlum.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCodAlum.Location = new System.Drawing.Point(296, 153);
            this.lblCodAlum.Name = "lblCodAlum";
            this.lblCodAlum.Size = new System.Drawing.Size(150, 28);
            this.lblCodAlum.TabIndex = 21;
            // 
            // lblNomAlum
            // 
            this.lblNomAlum.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblNomAlum.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNomAlum.Location = new System.Drawing.Point(296, 57);
            this.lblNomAlum.Name = "lblNomAlum";
            this.lblNomAlum.Size = new System.Drawing.Size(153, 28);
            this.lblNomAlum.TabIndex = 20;
            // 
            // btnCerrar
            // 
            this.btnCerrar.Location = new System.Drawing.Point(217, 267);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(116, 23);
            this.btnCerrar.TabIndex = 18;
            this.btnCerrar.Text = "Cerrar Sesion";
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // btnContenedor
            // 
            this.btnContenedor.Location = new System.Drawing.Point(61, 267);
            this.btnContenedor.Name = "btnContenedor";
            this.btnContenedor.Size = new System.Drawing.Size(116, 23);
            this.btnContenedor.TabIndex = 16;
            this.btnContenedor.Text = "Materias";
            this.btnContenedor.UseVisualStyleBackColor = true;
            this.btnContenedor.Click += new System.EventHandler(this.btnContenedor_Click);
            // 
            // picImagen
            // 
            this.picImagen.Location = new System.Drawing.Point(31, 29);
            this.picImagen.Name = "picImagen";
            this.picImagen.Size = new System.Drawing.Size(146, 179);
            this.picImagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picImagen.TabIndex = 15;
            this.picImagen.TabStop = false;
            // 
            // lblAlumno
            // 
            this.lblAlumno.AutoSize = true;
            this.lblAlumno.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlumno.Location = new System.Drawing.Point(201, 57);
            this.lblAlumno.Name = "lblAlumno";
            this.lblAlumno.Size = new System.Drawing.Size(90, 24);
            this.lblAlumno.TabIndex = 14;
            this.lblAlumno.Text = "Alumno:";
            // 
            // VentanaAlumno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 332);
            this.Controls.Add(this.lblUsAlum);
            this.Controls.Add(this.lblCuenta);
            this.Controls.Add(this.lblcod);
            this.Controls.Add(this.lblCodAlum);
            this.Controls.Add(this.lblNomAlum);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.btnContenedor);
            this.Controls.Add(this.picImagen);
            this.Controls.Add(this.lblAlumno);
            this.Name = "VentanaAlumno";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VentanaAlumno";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.VentanaAlumno_FormClosed);
            this.Load += new System.EventHandler(this.VentanaAlumno_Load);
            this.Controls.SetChildIndex(this.lblAlumno, 0);
            this.Controls.SetChildIndex(this.picImagen, 0);
            this.Controls.SetChildIndex(this.btnContenedor, 0);
            this.Controls.SetChildIndex(this.btnCerrar, 0);
            this.Controls.SetChildIndex(this.lblNomAlum, 0);
            this.Controls.SetChildIndex(this.lblCodAlum, 0);
            this.Controls.SetChildIndex(this.lblcod, 0);
            this.Controls.SetChildIndex(this.lblCuenta, 0);
            this.Controls.SetChildIndex(this.lblUsAlum, 0);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            ((System.ComponentModel.ISupportInitialize)(this.picImagen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUsAlum;
        private System.Windows.Forms.Label lblCuenta;
        private System.Windows.Forms.Label lblcod;
        private System.Windows.Forms.Label lblCodAlum;
        private System.Windows.Forms.Label lblNomAlum;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Button btnContenedor;
        private System.Windows.Forms.PictureBox picImagen;
        private System.Windows.Forms.Label lblAlumno;
    }
}