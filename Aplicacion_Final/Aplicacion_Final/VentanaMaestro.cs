﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace Aplicacion_Final
{
    public partial class VentanaMaestro : FormSalir
    {
        public VentanaMaestro()
        {
            InitializeComponent();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            VentanaLogin VenLo = new VentanaLogin();
            this.Hide();
            VenLo.Show();

        }

        private void VentanaMaestro_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void VentanaMaestro_Load(object sender, EventArgs e)
        {
            String cmd = "SELECT * FROM Usuario WHERE Id_Usuario=" + VentanaLogin.Codigo;

            DataSet DS = Utilidades.Ejecutar(cmd);

            lblNomMa.Text = DS.Tables[0].Rows[0]["Nom_Usuario"].ToString();
            lblUsMa.Text = DS.Tables[0].Rows[0]["Account"].ToString();
            lblCodMa.Text = DS.Tables[0].Rows[0]["Id_Usuario"].ToString();

            string url = DS.Tables[0].Rows[0]["Foto"].ToString();

            picImagen.Image = Image.FromFile(url);
        }

        private void btnCurso_Click(object sender, EventArgs e)
        {
           
        }

        private void btnContenedor_Click(object sender, EventArgs e)
        {
            ContenedorPrincipal ConP = new ContenedorPrincipal();
            this.Hide();
            ConP.Show();
        }
    }
}
