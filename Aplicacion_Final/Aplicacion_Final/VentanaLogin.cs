﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MiLibreria;
using System.Data;

namespace Aplicacion_Final
{
    public partial class VentanaLogin : FormSalir
    {
        public VentanaLogin()
        {
            InitializeComponent();
        }

        public static String Codigo = "";

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            try
            {
                string CMD = string.Format("Select * FROM Usuario Where Account='{0}' and Password='{1}'", txtCuenta.Text.Trim(), txtContra.Text.Trim());
                DataSet ds = Utilidades.Ejecutar(CMD);

                Codigo = ds.Tables[0].Rows[0]["Id_Usuario"].ToString().Trim();

                string cuenta = ds.Tables[0].Rows[0]["Account"].ToString().Trim();
                string contra = ds.Tables[0].Rows[0]["Password"].ToString().Trim();

                if (cuenta== txtCuenta.Text.Trim() && contra ==txtContra.Text.Trim())
                {
                    if (Convert.ToBoolean(ds.Tables[0].Rows[0]["Status_admin"])==true)
                    {
                        VentanaMaestro VenMa = new VentanaMaestro();
                        this.Hide();
                        VenMa.Show();
                    }
                    else
                    {
                        VentanaAlumno VenAlum = new VentanaAlumno();
                        this.Hide();
                        VenAlum.Show();
                    }
                }
            

            }
            catch(Exception )
            {
                MessageBox.Show("Usuario o Contraseña Incorrecta...!!");
            }
        }

        private void VentanaLogin_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
