﻿namespace Aplicacion_Final
{
    partial class AgregarMateria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNomMateria = new MiLibreria.ErrorTxtBox();
            this.txtIdMateria = new MiLibreria.ErrorTxtBox();
            this.lblMateria = new System.Windows.Forms.Label();
            this.lblMat = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(469, 123);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(469, 78);
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(469, 33);
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Location = new System.Drawing.Point(469, 173);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(469, 224);
            // 
            // txtNomMateria
            // 
            this.txtNomMateria.Location = new System.Drawing.Point(200, 129);
            this.txtNomMateria.Name = "txtNomMateria";
            this.txtNomMateria.Size = new System.Drawing.Size(213, 20);
            this.txtNomMateria.TabIndex = 22;
            this.txtNomMateria.Validar = true;
            // 
            // txtIdMateria
            // 
            this.txtIdMateria.Location = new System.Drawing.Point(200, 61);
            this.txtIdMateria.Name = "txtIdMateria";
            this.txtIdMateria.Size = new System.Drawing.Size(213, 20);
            this.txtIdMateria.TabIndex = 21;
            this.txtIdMateria.Validar = true;
            // 
            // lblMateria
            // 
            this.lblMateria.AutoSize = true;
            this.lblMateria.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMateria.Location = new System.Drawing.Point(21, 124);
            this.lblMateria.Name = "lblMateria";
            this.lblMateria.Size = new System.Drawing.Size(173, 24);
            this.lblMateria.TabIndex = 20;
            this.lblMateria.Text = "Nombre Profesor:";
            // 
            // lblMat
            // 
            this.lblMat.AutoSize = true;
            this.lblMat.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMat.Location = new System.Drawing.Point(71, 56);
            this.lblMat.Name = "lblMat";
            this.lblMat.Size = new System.Drawing.Size(123, 24);
            this.lblMat.TabIndex = 19;
            this.lblMat.Text = "ID Materia:";
            // 
            // AgregarMateria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 279);
            this.Controls.Add(this.txtNomMateria);
            this.Controls.Add(this.txtIdMateria);
            this.Controls.Add(this.lblMateria);
            this.Controls.Add(this.lblMat);
            this.Name = "AgregarMateria";
            this.Text = "AgregarMateria";
            this.Load += new System.EventHandler(this.AgregarMateria_Load);
            this.Controls.SetChildIndex(this.btnNuevo, 0);
            this.Controls.SetChildIndex(this.btnEliminar, 0);
            this.Controls.SetChildIndex(this.btnGuardar, 0);
            this.Controls.SetChildIndex(this.btnConsultar, 0);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.lblMat, 0);
            this.Controls.SetChildIndex(this.lblMateria, 0);
            this.Controls.SetChildIndex(this.txtIdMateria, 0);
            this.Controls.SetChildIndex(this.txtNomMateria, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MiLibreria.ErrorTxtBox txtNomMateria;
        private MiLibreria.ErrorTxtBox txtIdMateria;
        private System.Windows.Forms.Label lblMateria;
        private System.Windows.Forms.Label lblMat;
    }
}