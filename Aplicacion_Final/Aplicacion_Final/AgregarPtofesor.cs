﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace Aplicacion_Final
{
    public partial class AgregarPtofesor : Agregar
    {
        public AgregarPtofesor()
        {
            InitializeComponent();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                string cmd = string.Format("INSERT INTO dbo.Profesor (Id_Profesor, Nom_profesor, Apellidos_Profesor)" +
                "VALUES ('{0}','{1}','{2}')", txtIdProfesor.Text.Trim(), txtNomProfesor.Text.Trim(), txtApelliProfesor.Text.Trim());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se inserto un profesor con exito. " );
            }
            catch (Exception error)
            {
                MessageBox.Show("Error al insertar profesor. " + error.Message);
            }
            
        }
    }
}
