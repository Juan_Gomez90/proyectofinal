﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace Aplicacion_Final
{
    public partial class AgregarAlumno : Agregar
    {
        public AgregarAlumno()
        {
            InitializeComponent();
        }

        public override Boolean Guardar()
        {
          if (Utilidades.ValidarFormulario(this,errorProvider1)==false)
            {
                try
                {
                    string cmd = string.Format("EXEC ActualizaAlumno'{0}','{1}','{2}'", txtIdAlum.Text.Trim(), txtNomAlum.Text.Trim(), txtApelliAlum.Text.Trim());
                    Utilidades.Ejecutar(cmd);
                    MessageBox.Show("Se a guardado correctamente...!!");
                    return true;

                }
                catch (Exception error)
                {
                    MessageBox.Show("Ha ocurrido un error: " + error.Message);
                    return false;

                }
            }
          else
            {
                return false;
            }

        }
        public override void Eliminar()
        {
            try
            {
                string cmd = string.Format("EXEC EliminarAlumno '{0}'", txtIdAlum.Text.Trim());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se ha eliminado");
            }
            catch(Exception error)
            {
                MessageBox.Show("Ha ocurrido un error: " + error.Message);
            }
        }

        private void errorTxtBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtIdAlum_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.Clear();
        }
    }
}
