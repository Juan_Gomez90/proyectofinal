﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace Aplicacion_Final
{
    public partial class AgregarMateria : Agregar
    {
        public AgregarMateria()
        {
            InitializeComponent();
        }

        private void AgregarMateria_Load(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                string cmd = string.Format("INSERT INTO dbo.Materia (Id_Materia, Nom_Materia)" +
                "VALUES ('{0}','{1}')", txtIdMateria.Text.Trim(), txtNomMateria.Text.Trim());

                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se inserto una Materia con exito. ");
            }
            catch (Exception error)
            {
                MessageBox.Show("Error al insertar profesor. " + error.Message);
            }
        }
        public override void Eliminar()
        {
            try
            {
                string cmd = string.Format("EXEC EliminarMateria '{0}'", txtIdMateria.Text.Trim());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se ha eliminado");
            }
            catch (Exception error)
            {
                MessageBox.Show("Ha ocurrido un error: " + error.Message);
            }
        }

        private void errorTxtBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtIdAlum_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.Clear();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {

        }
    }
}

