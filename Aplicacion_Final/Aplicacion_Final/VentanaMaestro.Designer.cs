﻿namespace Aplicacion_Final
{
    partial class VentanaMaestro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMaestro = new System.Windows.Forms.Label();
            this.btnAdministrar = new System.Windows.Forms.Button();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.lblNomMa = new System.Windows.Forms.Label();
            this.lblCodMa = new System.Windows.Forms.Label();
            this.lblcod = new System.Windows.Forms.Label();
            this.lblCuenta = new System.Windows.Forms.Label();
            this.lblUsMa = new System.Windows.Forms.Label();
            this.picImagen = new System.Windows.Forms.PictureBox();
            this.btnContenedor = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picImagen)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(436, 279);
            this.btnSalir.Size = new System.Drawing.Size(91, 23);
            // 
            // lblMaestro
            // 
            this.lblMaestro.AutoSize = true;
            this.lblMaestro.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaestro.Location = new System.Drawing.Point(222, 69);
            this.lblMaestro.Name = "lblMaestro";
            this.lblMaestro.Size = new System.Drawing.Size(89, 24);
            this.lblMaestro.TabIndex = 0;
            this.lblMaestro.Text = "Maestro:";
            // 
            // btnAdministrar
            // 
            this.btnAdministrar.Location = new System.Drawing.Point(174, 279);
            this.btnAdministrar.Name = "btnAdministrar";
            this.btnAdministrar.Size = new System.Drawing.Size(116, 23);
            this.btnAdministrar.TabIndex = 5;
            this.btnAdministrar.Text = "Administrar Alumnos";
            this.btnAdministrar.UseVisualStyleBackColor = true;
            // 
            // btnCerrar
            // 
            this.btnCerrar.Location = new System.Drawing.Point(298, 279);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(116, 23);
            this.btnCerrar.TabIndex = 6;
            this.btnCerrar.Text = "Cerrar Sesion";
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // lblNomMa
            // 
            this.lblNomMa.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblNomMa.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNomMa.Location = new System.Drawing.Point(317, 69);
            this.lblNomMa.Name = "lblNomMa";
            this.lblNomMa.Size = new System.Drawing.Size(153, 28);
            this.lblNomMa.TabIndex = 8;
            // 
            // lblCodMa
            // 
            this.lblCodMa.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblCodMa.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCodMa.Location = new System.Drawing.Point(317, 165);
            this.lblCodMa.Name = "lblCodMa";
            this.lblCodMa.Size = new System.Drawing.Size(150, 28);
            this.lblCodMa.TabIndex = 10;
            // 
            // lblcod
            // 
            this.lblcod.AutoSize = true;
            this.lblcod.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcod.Location = new System.Drawing.Point(234, 165);
            this.lblcod.Name = "lblcod";
            this.lblcod.Size = new System.Drawing.Size(77, 24);
            this.lblcod.TabIndex = 11;
            this.lblcod.Text = "Codigo:";
            // 
            // lblCuenta
            // 
            this.lblCuenta.AutoSize = true;
            this.lblCuenta.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCuenta.Location = new System.Drawing.Point(221, 110);
            this.lblCuenta.Name = "lblCuenta";
            this.lblCuenta.Size = new System.Drawing.Size(90, 24);
            this.lblCuenta.TabIndex = 12;
            this.lblCuenta.Text = "Usuario:";
            // 
            // lblUsMa
            // 
            this.lblUsMa.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblUsMa.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUsMa.Location = new System.Drawing.Point(317, 110);
            this.lblUsMa.Name = "lblUsMa";
            this.lblUsMa.Size = new System.Drawing.Size(153, 28);
            this.lblUsMa.TabIndex = 13;
            // 
            // picImagen
            // 
            this.picImagen.Location = new System.Drawing.Point(52, 41);
            this.picImagen.Name = "picImagen";
            this.picImagen.Size = new System.Drawing.Size(146, 179);
            this.picImagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picImagen.TabIndex = 3;
            this.picImagen.TabStop = false;
            // 
            // btnContenedor
            // 
            this.btnContenedor.Location = new System.Drawing.Point(39, 279);
            this.btnContenedor.Name = "btnContenedor";
            this.btnContenedor.Size = new System.Drawing.Size(116, 23);
            this.btnContenedor.TabIndex = 14;
            this.btnContenedor.Text = "Contenedor Principal";
            this.btnContenedor.UseVisualStyleBackColor = true;
            this.btnContenedor.Click += new System.EventHandler(this.btnContenedor_Click);
            // 
            // VentanaMaestro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 338);
            this.Controls.Add(this.btnContenedor);
            this.Controls.Add(this.lblUsMa);
            this.Controls.Add(this.lblCuenta);
            this.Controls.Add(this.lblcod);
            this.Controls.Add(this.lblCodMa);
            this.Controls.Add(this.lblNomMa);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.btnAdministrar);
            this.Controls.Add(this.picImagen);
            this.Controls.Add(this.lblMaestro);
            this.Name = "VentanaMaestro";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VentanaMaestro";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.VentanaMaestro_FormClosed);
            this.Load += new System.EventHandler(this.VentanaMaestro_Load);
            this.Controls.SetChildIndex(this.lblMaestro, 0);
            this.Controls.SetChildIndex(this.picImagen, 0);
            this.Controls.SetChildIndex(this.btnAdministrar, 0);
            this.Controls.SetChildIndex(this.btnCerrar, 0);
            this.Controls.SetChildIndex(this.lblNomMa, 0);
            this.Controls.SetChildIndex(this.lblCodMa, 0);
            this.Controls.SetChildIndex(this.lblcod, 0);
            this.Controls.SetChildIndex(this.lblCuenta, 0);
            this.Controls.SetChildIndex(this.lblUsMa, 0);
            this.Controls.SetChildIndex(this.btnContenedor, 0);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            ((System.ComponentModel.ISupportInitialize)(this.picImagen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMaestro;
        private System.Windows.Forms.PictureBox picImagen;
        private System.Windows.Forms.Button btnAdministrar;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Label lblNomMa;
        private System.Windows.Forms.Label lblCodMa;
        private System.Windows.Forms.Label lblcod;
        private System.Windows.Forms.Label lblCuenta;
        private System.Windows.Forms.Label lblUsMa;
        private System.Windows.Forms.Button btnContenedor;
    }
}