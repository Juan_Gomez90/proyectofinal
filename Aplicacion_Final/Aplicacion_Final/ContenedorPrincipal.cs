﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicacion_Final
{
    public partial class ContenedorPrincipal : Form
    {
        private int childFormNumber = 0;

        public ContenedorPrincipal()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Ventana " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }



        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void alumnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AgregarAlumno AgreAlum = new AgregarAlumno();
            AgreAlum.MdiParent = this;
            AgreAlum.Show();
        }

        private void alumnoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ConsultarAlumno ConAlum = new ConsultarAlumno();
            ConAlum.MdiParent = this;
            ConAlum.Show();

         
        }

        private void calificacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Calificaciones Cal = new Calificaciones();
            Cal.MdiParent = this;
            Cal.Show();
        }

        private void ContenedorPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void profesoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AgregarPtofesor AgrePro = new AgregarPtofesor();
            AgrePro.MdiParent = this;
            AgrePro.Show();
        }

        private void materiaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AgregarMateria AgreMat = new AgregarMateria();
            AgreMat.MdiParent = this;
            AgreMat.Show();
        }

        private void materiaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ConsultarMateria ConMat = new ConsultarMateria();
            ConMat.MdiParent = this;
            ConMat.Show();
        }

        private void profesorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarProfesor ConPro = new ConsultarProfesor();
            ConPro.MdiParent = this;
            ConPro.Show();

        }
    }
}
