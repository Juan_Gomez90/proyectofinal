﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace Aplicacion_Final
{
    public partial class ConsultarMateria : Consultas
    {
        public ConsultarMateria()
        {
            InitializeComponent();
        }

        private void ConsultarMateria_Load(object sender, EventArgs e)
        {
            ConsultarInfo.DataSource = LLenarDataGV("Materia").Tables[0];
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtAlumno.Text.Trim()) == false)
            {
                try
                {
                    DataSet ds;
                    string cmd = "Select * FROM Materia WHERE  Nom_Materia LIKE ('%" + txtAlumno.Text.Trim() + "%')";

                    ds = Utilidades.Ejecutar(cmd);
                    ConsultarInfo.DataSource = ds.Tables[0];
                }
                catch (Exception error)
                {
                    MessageBox.Show("Ha ocurrido un error: " + error.Message);
                }
            }
        }
    }
}
