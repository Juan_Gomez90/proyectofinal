﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace Aplicacion_Final
{
    public partial class Consultas : FormSalir
    {
        public Consultas()
        {
            InitializeComponent();
        }

        public DataSet LLenarDataGV(string tabla)
        {
            DataSet DS;
            string cmd = string.Format("SELECT * FROM " + tabla);
            DS = Utilidades.Ejecutar(cmd);

            return DS;
        }

        private void btnSeleccionar_Click(object sender, EventArgs e)
        {
            if(ConsultarInfo.Rows.Count== 0)
            {
                return;
            }
            else
            {
                DialogResult = DialogResult.OK;
                Close();

            }
        }
    }
}
