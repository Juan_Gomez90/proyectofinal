﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace Aplicacion_Final
{
    public partial class Calificaciones : Procesos
    {
        public Calificaciones()
        {
            InitializeComponent();
        }

        private void Calificaciones_Load(object sender, EventArgs e)
        {
            string cmd = "Select * From Usuario where Id_Usuario=" + VentanaLogin.Codigo;
            DataSet ds = Utilidades.Ejecutar(cmd);

            lblLeAtiende.Text = ds.Tables[0].Rows[0]["Nom_Usuario"].ToString().Trim();
        }
        public static int cont_fila = 0;
        public static double total;
        private void btnColocar_Click(object sender, EventArgs e)
        {
            if(Utilidades.ValidarFormulario(this,errorProvider1)== false)
            {
                bool existe = false;
                int num_fila = 0;

                if (cont_fila==0)
                {
                    dtgCalificaciones.Rows.Add(txtCodigo.Text, txtNombre.Text, txtApellidos.Text,txtCalificacion1.Text,txtCalificacion2.Text);
                    double total = (Convert.ToDouble(dtgCalificaciones.Rows[cont_fila].Cells[3].Value) + Convert.ToDouble(dtgCalificaciones.Rows[cont_fila].Cells[4].Value)) / 2;
                    dtgCalificaciones.Rows[cont_fila].Cells[5].Value = total;

                    cont_fila++;

                }
                else
                {
                    foreach(DataGridViewRow Fila in dtgCalificaciones.Rows)
                    {
                        if (Fila.Cells[0].Value.ToString() == txtCodigo.Text)
                        {
                            existe = true;
                            num_fila = Fila.Index;
                        }
                    }
                    if(existe == true)
                    {
                        dtgCalificaciones.Rows[num_fila].Cells[3].Value = (Convert.ToDouble(txtCalificacion1.Text) * Convert.ToDouble(dtgCalificaciones.Rows[num_fila].Cells[3].Value)).ToString();
                        dtgCalificaciones.Rows[num_fila].Cells[4].Value = (Convert.ToDouble(txtCalificacion1.Text) * Convert.ToDouble(dtgCalificaciones.Rows[num_fila].Cells[4].Value)).ToString();
                        double total = (Convert.ToDouble(dtgCalificaciones.Rows[num_fila].Cells[3].Value) + Convert.ToDouble(dtgCalificaciones.Rows[num_fila].Cells[4].Value))/2 ;
                        dtgCalificaciones.Rows[num_fila].Cells[5].Value = total;


                    }
                    else
                    {
                        dtgCalificaciones.Rows.Add(txtCodigo.Text, txtNombre.Text, txtApellidos.Text, txtCalificacion1.Text, txtCalificacion2.Text);
                        double total = (Convert.ToDouble(dtgCalificaciones.Rows[cont_fila].Cells[3].Value) + Convert.ToDouble(dtgCalificaciones.Rows[cont_fila].Cells[4].Value))/ 2;
                        dtgCalificaciones.Rows[cont_fila].Cells[5].Value = total;

                        cont_fila++;
                    }
                }

                total = 0;
                foreach (DataGridViewRow Fila in dtgCalificaciones.Rows)
                {
                    total += Convert.ToDouble(Fila.Cells[5].Value);

                }

                lblTotalfinal.Text = "Promedio De salon: " + total.ToString();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if(cont_fila >0)
            {
                total = total - (Convert.ToDouble(dtgCalificaciones.Rows[dtgCalificaciones.CurrentRow.Index].Cells[5].Value));
                lblPromedio.Text = "Promedio De salon: " + total.ToString();
                dtgCalificaciones.Rows.RemoveAt(dtgCalificaciones.CurrentRow.Index);
                cont_fila--;
            }
        }

        private void btnAlumnos_Click(object sender, EventArgs e)
        {
            ConsultarAlumno Ca = new ConsultarAlumno();
            Ca.ShowDialog();
            if(Ca.DialogResult == DialogResult.OK)
            {
                txtCodigo.Text = Ca.ConsultarInfo.Rows[Ca.ConsultarInfo.CurrentRow.Index].Cells[0].Value.ToString();
                txtNombre.Text = Ca.ConsultarInfo.Rows[Ca.ConsultarInfo.CurrentRow.Index].Cells[1].Value.ToString();
                txtApellidos.Text = Ca.ConsultarInfo.Rows[Ca.ConsultarInfo.CurrentRow.Index].Cells[2].Value.ToString();
            }

        

        }
    }
}
