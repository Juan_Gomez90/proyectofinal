﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace Aplicacion_Final
{
    public partial class ConsultarProfesor : Consultas
    {
        public ConsultarProfesor()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtAlumno.Text.Trim()) == false)
            {
                try
                {
                    DataSet ds;
                    string cmd = "Select * FROM Profesor WHERE  Nom_profesor LIKE ('%" + txtAlumno.Text.Trim() + "%')";

                    ds = Utilidades.Ejecutar(cmd);
                    ConsultarInfo.DataSource = ds.Tables[0];
                }
                catch (Exception error)
                {
                    MessageBox.Show("Ha ocurrido un error: " + error.Message);
                }
            }
        }

        private void ConsultarProfesor_Load(object sender, EventArgs e)
        {
            ConsultarInfo.DataSource = LLenarDataGV("Profesor").Tables[0];
        }

        private void btnSeleccionar_Click(object sender, EventArgs e)
        {
            if (DialogResult == DialogResult.OK)
            {
                
            }
            
        }
    }
}
