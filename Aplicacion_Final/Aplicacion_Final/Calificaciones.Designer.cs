﻿namespace Aplicacion_Final
{
    partial class Calificaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblMaestro = new System.Windows.Forms.Label();
            this.txtApellidos = new MiLibreria.ErrorTxtBox();
            this.txtNombre = new MiLibreria.ErrorTxtBox();
            this.txtCalificacion1 = new MiLibreria.ErrorTxtBox();
            this.lblCalificacion1 = new System.Windows.Forms.Label();
            this.lblApellidos = new System.Windows.Forms.Label();
            this.lblAlumno = new System.Windows.Forms.Label();
            this.LblCodigo = new System.Windows.Forms.Label();
            this.txtCodigo = new MiLibreria.ErrorTxtBox();
            this.dtgCalificaciones = new System.Windows.Forms.DataGridView();
            this.ColCodigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColApellidos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCalificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCalificaicon2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colResultado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnColocar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnAlumnos = new System.Windows.Forms.Button();
            this.lblPromedio = new System.Windows.Forms.Label();
            this.lblTotalfinal = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.lblLeAtiende = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.lblCalificacion2 = new System.Windows.Forms.Label();
            this.txtCalificacion2 = new MiLibreria.ErrorTxtBox();
            this.lblTotal = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtgCalificaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(173, 415);
            this.btnSalir.Size = new System.Drawing.Size(120, 23);
            // 
            // lblMaestro
            // 
            this.lblMaestro.AutoSize = true;
            this.lblMaestro.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaestro.Location = new System.Drawing.Point(626, 38);
            this.lblMaestro.Name = "lblMaestro";
            this.lblMaestro.Size = new System.Drawing.Size(83, 24);
            this.lblMaestro.TabIndex = 3;
            this.lblMaestro.Text = "Maestro";
            // 
            // txtApellidos
            // 
            this.txtApellidos.Location = new System.Drawing.Point(387, 153);
            this.txtApellidos.Name = "txtApellidos";
            this.txtApellidos.Size = new System.Drawing.Size(168, 20);
            this.txtApellidos.TabIndex = 6;
            this.txtApellidos.Validar = true;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(141, 153);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(220, 20);
            this.txtNombre.TabIndex = 7;
            this.txtNombre.Validar = true;
            // 
            // txtCalificacion1
            // 
            this.txtCalificacion1.Location = new System.Drawing.Point(576, 153);
            this.txtCalificacion1.Name = "txtCalificacion1";
            this.txtCalificacion1.Size = new System.Drawing.Size(109, 20);
            this.txtCalificacion1.TabIndex = 8;
            this.txtCalificacion1.Validar = true;
            // 
            // lblCalificacion1
            // 
            this.lblCalificacion1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblCalificacion1.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCalificacion1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblCalificacion1.Location = new System.Drawing.Point(572, 176);
            this.lblCalificacion1.Name = "lblCalificacion1";
            this.lblCalificacion1.Size = new System.Drawing.Size(137, 34);
            this.lblCalificacion1.TabIndex = 10;
            this.lblCalificacion1.Text = "1er Cuatri";
            // 
            // lblApellidos
            // 
            this.lblApellidos.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblApellidos.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApellidos.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblApellidos.Location = new System.Drawing.Point(392, 176);
            this.lblApellidos.Name = "lblApellidos";
            this.lblApellidos.Size = new System.Drawing.Size(185, 34);
            this.lblApellidos.TabIndex = 11;
            this.lblApellidos.Text = "Apellidos";
            // 
            // lblAlumno
            // 
            this.lblAlumno.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblAlumno.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlumno.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblAlumno.Location = new System.Drawing.Point(207, 176);
            this.lblAlumno.Name = "lblAlumno";
            this.lblAlumno.Size = new System.Drawing.Size(187, 34);
            this.lblAlumno.TabIndex = 12;
            this.lblAlumno.Text = "Nombre";
            // 
            // LblCodigo
            // 
            this.LblCodigo.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.LblCodigo.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCodigo.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LblCodigo.Location = new System.Drawing.Point(30, 176);
            this.LblCodigo.Name = "LblCodigo";
            this.LblCodigo.Size = new System.Drawing.Size(178, 34);
            this.LblCodigo.TabIndex = 13;
            this.LblCodigo.Text = "Codigo";
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(30, 153);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(105, 20);
            this.txtCodigo.TabIndex = 14;
            this.txtCodigo.Validar = true;
            // 
            // dtgCalificaciones
            // 
            this.dtgCalificaciones.AllowUserToAddRows = false;
            this.dtgCalificaciones.AllowUserToDeleteRows = false;
            this.dtgCalificaciones.AllowUserToResizeColumns = false;
            this.dtgCalificaciones.AllowUserToResizeRows = false;
            this.dtgCalificaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgCalificaciones.ColumnHeadersVisible = false;
            this.dtgCalificaciones.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColCodigo,
            this.ColNombre,
            this.ColApellidos,
            this.ColCalificacion,
            this.ColCalificaicon2,
            this.colResultado});
            this.dtgCalificaciones.Location = new System.Drawing.Point(30, 203);
            this.dtgCalificaciones.Name = "dtgCalificaciones";
            this.dtgCalificaciones.ReadOnly = true;
            this.dtgCalificaciones.RowHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.dtgCalificaciones.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgCalificaciones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgCalificaciones.Size = new System.Drawing.Size(914, 130);
            this.dtgCalificaciones.TabIndex = 15;
            // 
            // ColCodigo
            // 
            this.ColCodigo.HeaderText = "Codigo";
            this.ColCodigo.Name = "ColCodigo";
            this.ColCodigo.ReadOnly = true;
            // 
            // ColNombre
            // 
            this.ColNombre.HeaderText = "Nombre";
            this.ColNombre.Name = "ColNombre";
            this.ColNombre.ReadOnly = true;
            this.ColNombre.Width = 250;
            // 
            // ColApellidos
            // 
            this.ColApellidos.HeaderText = "Apellidos";
            this.ColApellidos.Name = "ColApellidos";
            this.ColApellidos.ReadOnly = true;
            this.ColApellidos.Width = 180;
            // 
            // ColCalificacion
            // 
            this.ColCalificacion.HeaderText = "Calificacion";
            this.ColCalificacion.Name = "ColCalificacion";
            this.ColCalificacion.ReadOnly = true;
            this.ColCalificacion.Width = 140;
            // 
            // ColCalificaicon2
            // 
            this.ColCalificaicon2.HeaderText = "Calificacion2";
            this.ColCalificaicon2.Name = "ColCalificaicon2";
            this.ColCalificaicon2.ReadOnly = true;
            this.ColCalificaicon2.Width = 140;
            // 
            // colResultado
            // 
            this.colResultado.HeaderText = "Resultado";
            this.colResultado.Name = "colResultado";
            this.colResultado.ReadOnly = true;
            // 
            // btnColocar
            // 
            this.btnColocar.Location = new System.Drawing.Point(30, 339);
            this.btnColocar.Name = "btnColocar";
            this.btnColocar.Size = new System.Drawing.Size(120, 23);
            this.btnColocar.TabIndex = 16;
            this.btnColocar.Text = "Colocar";
            this.btnColocar.UseVisualStyleBackColor = true;
            this.btnColocar.Click += new System.EventHandler(this.btnColocar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(173, 339);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(120, 23);
            this.btnEliminar.TabIndex = 17;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnAlumnos
            // 
            this.btnAlumnos.Location = new System.Drawing.Point(30, 378);
            this.btnAlumnos.Name = "btnAlumnos";
            this.btnAlumnos.Size = new System.Drawing.Size(120, 23);
            this.btnAlumnos.TabIndex = 18;
            this.btnAlumnos.Text = "Alumnos";
            this.btnAlumnos.UseVisualStyleBackColor = true;
            this.btnAlumnos.Click += new System.EventHandler(this.btnAlumnos_Click);
            // 
            // lblPromedio
            // 
            this.lblPromedio.AutoSize = true;
            this.lblPromedio.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPromedio.Location = new System.Drawing.Point(370, 396);
            this.lblPromedio.Name = "lblPromedio";
            this.lblPromedio.Size = new System.Drawing.Size(162, 24);
            this.lblPromedio.TabIndex = 19;
            this.lblPromedio.Text = "Promedio Total:";
            // 
            // lblTotalfinal
            // 
            this.lblTotalfinal.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTotalfinal.Location = new System.Drawing.Point(538, 383);
            this.lblTotalfinal.Name = "lblTotalfinal";
            this.lblTotalfinal.Size = new System.Drawing.Size(192, 37);
            this.lblTotalfinal.TabIndex = 20;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Aplicacion_Final.Properties.Resources.logoSiga;
            this.pictureBox1.Location = new System.Drawing.Point(113, 25);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(235, 98);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // btnNuevo
            // 
            this.btnNuevo.Location = new System.Drawing.Point(173, 378);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(120, 23);
            this.btnNuevo.TabIndex = 21;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.UseVisualStyleBackColor = true;
            // 
            // lblLeAtiende
            // 
            this.lblLeAtiende.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.lblLeAtiende.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLeAtiende.Location = new System.Drawing.Point(558, 73);
            this.lblLeAtiende.Name = "lblLeAtiende";
            this.lblLeAtiende.Size = new System.Drawing.Size(213, 37);
            this.lblLeAtiende.TabIndex = 22;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // lblCalificacion2
            // 
            this.lblCalificacion2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblCalificacion2.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCalificacion2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblCalificacion2.Location = new System.Drawing.Point(703, 176);
            this.lblCalificacion2.Name = "lblCalificacion2";
            this.lblCalificacion2.Size = new System.Drawing.Size(139, 34);
            this.lblCalificacion2.TabIndex = 23;
            this.lblCalificacion2.Text = "2do Cuatri";
            // 
            // txtCalificacion2
            // 
            this.txtCalificacion2.Location = new System.Drawing.Point(701, 153);
            this.txtCalificacion2.Name = "txtCalificacion2";
            this.txtCalificacion2.Size = new System.Drawing.Size(141, 20);
            this.txtCalificacion2.TabIndex = 24;
            this.txtCalificacion2.Validar = true;
            // 
            // lblTotal
            // 
            this.lblTotal.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblTotal.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblTotal.Location = new System.Drawing.Point(839, 176);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(105, 34);
            this.lblTotal.TabIndex = 25;
            this.lblTotal.Text = "Total";
            // 
            // Calificaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(988, 439);
            this.Controls.Add(this.txtCalificacion2);
            this.Controls.Add(this.lblLeAtiende);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.lblTotalfinal);
            this.Controls.Add(this.lblPromedio);
            this.Controls.Add(this.btnAlumnos);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnColocar);
            this.Controls.Add(this.dtgCalificaciones);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.LblCodigo);
            this.Controls.Add(this.lblAlumno);
            this.Controls.Add(this.lblApellidos);
            this.Controls.Add(this.lblCalificacion1);
            this.Controls.Add(this.txtCalificacion1);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.txtApellidos);
            this.Controls.Add(this.lblMaestro);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblCalificacion2);
            this.Controls.Add(this.lblTotal);
            this.Name = "Calificaciones";
            this.Text = "Calificaciones";
            this.Load += new System.EventHandler(this.Calificaciones_Load);
            this.Controls.SetChildIndex(this.lblTotal, 0);
            this.Controls.SetChildIndex(this.lblCalificacion2, 0);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.pictureBox1, 0);
            this.Controls.SetChildIndex(this.lblMaestro, 0);
            this.Controls.SetChildIndex(this.txtApellidos, 0);
            this.Controls.SetChildIndex(this.txtNombre, 0);
            this.Controls.SetChildIndex(this.txtCalificacion1, 0);
            this.Controls.SetChildIndex(this.lblCalificacion1, 0);
            this.Controls.SetChildIndex(this.lblApellidos, 0);
            this.Controls.SetChildIndex(this.lblAlumno, 0);
            this.Controls.SetChildIndex(this.LblCodigo, 0);
            this.Controls.SetChildIndex(this.txtCodigo, 0);
            this.Controls.SetChildIndex(this.dtgCalificaciones, 0);
            this.Controls.SetChildIndex(this.btnColocar, 0);
            this.Controls.SetChildIndex(this.btnEliminar, 0);
            this.Controls.SetChildIndex(this.btnAlumnos, 0);
            this.Controls.SetChildIndex(this.lblPromedio, 0);
            this.Controls.SetChildIndex(this.lblTotalfinal, 0);
            this.Controls.SetChildIndex(this.btnNuevo, 0);
            this.Controls.SetChildIndex(this.lblLeAtiende, 0);
            this.Controls.SetChildIndex(this.txtCalificacion2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dtgCalificaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblMaestro;
        private MiLibreria.ErrorTxtBox txtApellidos;
        private MiLibreria.ErrorTxtBox txtNombre;
        private MiLibreria.ErrorTxtBox txtCalificacion1;
        private System.Windows.Forms.Label lblCalificacion1;
        private System.Windows.Forms.Label lblApellidos;
        private System.Windows.Forms.Label lblAlumno;
        private System.Windows.Forms.Label LblCodigo;
        private MiLibreria.ErrorTxtBox txtCodigo;
        private System.Windows.Forms.Button btnColocar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnAlumnos;
        private System.Windows.Forms.Label lblPromedio;
        private System.Windows.Forms.Label lblTotalfinal;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.Label lblLeAtiende;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label lblCalificacion2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCodigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColApellidos;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCalificacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCalificaicon2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colResultado;
        private MiLibreria.ErrorTxtBox txtCalificacion2;
        private System.Windows.Forms.Label lblTotal;
        public System.Windows.Forms.DataGridView dtgCalificaciones;
    }
}